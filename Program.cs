﻿using System;
//Microsoft Machine Learning package is used in order to use logistic regression
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Trainers;

namespace Predictor
{
    class Program
    {
        static void Main(string[] args)
        {
            //Initalize ML.NET environment
            MLContext mlContext = new MLContext();

            //Load training data where each row is seperated by ;
            IDataView trainingData = mlContext.Data.LoadFromTextFile<ModelInput>("cardio.csv", separatorChar: ';');

            //Create pipeline and choose features to train with (except prediction attribute - cardio)
            var pipeline = mlContext.Transforms.Concatenate("Features", new[]
            { "age", "gender", "height", "weight", "ap_hi", "ap_lo", "cholesterol", "gluc", "smoke", "alco", "active" });

            //Start a model using logistic regression with prediction attribute
            Console.WriteLine("Creating a logistic regression model");
            var options =
              new LbfgsLogisticRegressionBinaryTrainer.Options()
              {
                  LabelColumnName = "cardio",
                  FeatureColumnName = "Features",
                  MaximumNumberOfIterations = 100,
                  OptimizationTolerance = 1e-8f
              };

            //Apply logistic regression to model and train with given data
            var trainer = mlContext.BinaryClassification.Trainers.LbfgsLogisticRegression(options);
            var trainPipe = pipeline.Append(trainer);
            Console.WriteLine("Starting training...");

            //Fit training data into model
            ITransformer model = trainPipe.Fit(trainingData);
            Console.WriteLine("Training completed");
            Console.WriteLine();

            //Start getting inputs from user in order to do prediction
            Console.WriteLine("Starting your prediction questions");
            Console.WriteLine();

            Console.Write("Enter Age in Years and Press Enter: ");
            Single age = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine();

            Console.Write("Enter gender as int (1-Female, 2-Male) and Press Enter: ");
            Single gender = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine();

            Console.Write("Enter height in cm and Press Enter: ");
            Single height = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine();

            Console.Write("Enter Weight in kg and Press Enter: ");
            Single weight = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine();

            Console.Write("Enter High Blood Pressure and Press Enter: ");
            Single ap_hi = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine();

            Console.Write("Enter Low Blood Pressure Press Enter: ");
            Single ap_lo = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("1: normal, 2: above normal, 3: high ");
            Console.Write("Enter number representing your Cholesterol and Press Enter: ");
            Single cholesterol = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("1: normal, 2: above normal, 3: high ");
            Console.Write("Enter number representing your Glucose and Press Enter: ");
            Single gluc = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("0: no, 1: yes");
            Console.Write("Do you smoke (0/1): ");
            Single smoke = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("0: no, 1: yes");
            Console.Write("Do you drink (0/1): ");
            Single alco = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("0: no, 1: yes");
            Console.Write("Are you active (0/1): ");
            Single active = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine();

            //Prediction Model - specify user input to prediction data
            ModelInput X = new ModelInput()
            {
                age = age,
                gender = gender,
                height = height,
                weight = weight,
                ap_hi = ap_hi,
                ap_lo = ap_lo,
                cholesterol = cholesterol,
                gluc = gluc,
                smoke = smoke,
                alco = alco,
                active = active,
            };

            //Making a Prediction
            Console.WriteLine("Starting Prediction...");
            var pe = mlContext.Model.CreatePredictionEngine<ModelInput, Prediction>(model);
            var Y = pe.Predict(X);
            Console.Write("Predicted Presence of Cardiovascular Disease : ");
            Console.WriteLine(Y.Predicted);
        }

        //Model class retrieves all the attributes from each dataset column
        public class ModelInput
        {
            [LoadColumn(1)]
            public Single age;
            [LoadColumn(2)]
            public Single gender;
            [LoadColumn(3)]
            public Single height;
            [LoadColumn(4)]
            public Single weight;
            [LoadColumn(5)]
            public Single ap_hi;
            [LoadColumn(6)]
            public Single ap_lo;
            [LoadColumn(7)]
            public Single cholesterol;
            [LoadColumn(8)]
            public Single gluc;
            [LoadColumn(9)]
            public Single smoke;
            [LoadColumn(10)]
            public Single alco;
            [LoadColumn(11)]
            public Single active;
            [LoadColumn(12)]
            public bool cardio;
        }
        //Stores the prediction
        public class Prediction
        {
            [ColumnName("PredictedLabel")] public bool Predicted;
            [ColumnName("Score")] public float Distances;
        }
    }
}
